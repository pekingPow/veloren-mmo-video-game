## Régénération

buff-title-heal = Soin
buff-desc-heal = Régénère progressivement des points de vie.
buff-stat-health = Restaure { $str_total } points de vie

## Potion

buff-title-potion = Potion
buff-desc-potion = En train de boire...

## Saturation

buff-title-saturation = Saturation
buff-desc-saturation = Régénère progressivement des points de vie grâce à la nourriture.

## Feu de camp

buff-title-campfire_heal = Soin autour d'un feu de camp
buff-desc-campfire_heal = Se reposer à côté d'un feu de camp restaure { $rate }% de santé chaque seconde.

## Régen d'Endurance

buff-title-energy_regen = Régénération d'Endurance
buff-desc-energy_regen = Régénération de l'Endurance plus rapide
buff-stat-energy_regen = Restaure { $str_total } d'Endurance

## Augmentation de Santé

buff-title-increase_max_health = Augmentation de Santé
buff-desc-increase_max_health = Augmente votre limite max de points de vie
buff-stat-increase_max_health =
    Augmente les points de vie max
    de { $strength }

## Augmentation d'Endurance

buff-title-increase_max_energy = Augmentation d'Endurance
buff-desc-increase_max_energy = Augmente votre limite maximale de points d'endurance
buff-stat-increase_max_energy =
    Augmente les points d'endurance max
    de { $strength }

## Invulnérabilité

buff-title-invulnerability = Invulnérabilité
buff-desc-invulnerability = Vous ne pouvez pas être blessé par une attaque.
buff-stat-invulnerability = Rend invincible

## Aura de Protection

buff-title-protectingward = Aura de Protection
buff-desc-protectingward = Vous êtes protégé, d'une quelconque façon, des attaques ennemies

## Frénésie

buff-title-frenzied = Frénétique
buff-desc-frenzied = Vous bénéficiez d'une vitesse surnaturelle et ignorez les blessures superficielles.

## Hâte

buff-title-hastened = Hâte
buff-desc-hastened = Vos mouvements et vos attaques sont plus rapides.

## Saignement

buff-title-bleed = Saignement
buff-desc-bleed = Inflige régulièrement des dommages.

## Malédiction

buff-title-cursed = Maudit
buff-desc-cursed = Vous êtes maudit.

## Brûlure

buff-title-burn = En feu
buff-desc-burn = Vous êtes en train de brûler vivant.

## Estropié

buff-title-crippled = Estropie
buff-desc-crippled = Vos mouvements sont ralentis suite à de graves blessures aux jambes.

## Gelé

buff-title-frozen = Glacé(e)
buff-desc-frozen = Vos mouvements et attaques sont ralentis.

## Trempé

buff-title-wet = Trempé(e)
buff-desc-wet = Le sol rejette vos pieds, rendant le fait de s'arrêter difficile.

## Enchaîné

buff-title-ensnared = Piégé(e)
buff-desc-ensnared = Des plantes grimpantes s'attachent à vos jambes, restreignant vos mouvements.

## Fortitude

buff-title-fortitude = Courage
buff-desc-fortitude = Vous pouvez résister aux étourdissements, et plus vous prenez de dégâts, plus vous étourdissez les autres facilement.

## Paré

buff-title-parried = Paré
buff-desc-parried = Tu as été paré et tu es maintenant lent à récupérer.

## Util

buff-text-over_seconds = pendant { $dur_secs } secondes
buff-text-for_seconds = pour { $dur_secs } secondes
buff-remove = Cliquer pour retirer
# Reckless
buff-title-reckless = Imprudent
# Potion sickness
buff-title-potionsickness = Mal des potions
# Potion sickness
buff-desc-potionsickness = Les potions vous guérissent moins après en avoir consommé.
# Reckless
buff-desc-reckless = Vos attaques sont plus puissantes mais vous laissez vos défenses ouvertes.
# Lifesteal
buff-desc-lifesteal = Siphonne la vie de vos ennemis.
# Polymorped
buff-title-polymorphed = Polymorphe
# Salamander's Aspect
buff-title-salamanderaspect = Allure des salamandres
# Polymorped
buff-desc-polymorphed = Votre corps change de forme.
# Frigid
buff-title-frigid = Glacé
# Frigid
buff-desc-frigid = Gèle vos ennemis.
# Flame
buff-desc-flame = Les flammes sont vos alliés.
# Potion sickness
buff-stat-potionsickness =
    Réduit la quantité de vie regagnée par
    les potions suivantes de { $strength }%.
# Imminent Critical
buff-title-imminentcritical = Coup critique imminent
# Bloodfeast
buff-title-bloodfeast = Fête du sang
# Imminent Critical
buff-desc-imminentcritical = Votre prochaine attaque frappera votre ennemie de manière critique.
# Fury
buff-desc-fury = Avec votre fureur, vos coups génèrent plus de combo
# Sunderer
buff-desc-defiance = Vous pouvez résister à des coups plus puissants et étourdissants et générer des combos en étant frappé mais vous êtes plus lent.
# Bloodfeast
buff-desc-bloodfeast = Vous regagner de la vie en attaquant des ennemis saignant
# Sunderer
buff-title-sunderer = Fragmentation
# Salamander's Aspect
buff-desc-salamanderaspect = Vous ne pouvez pas brûler et avancez vite dans la lave.
# Fury
buff-title-fury = Fureur
# Sunderer
buff-desc-sunderer = Vos attaques peuvent transpercer les défenses de vos ennemis et vous redonner plus d'énergie.
# Berserk
buff-title-berserk = Fou furieux
# Berserk
buff-desc-berserk = Vous êtes dans une rage furieuse, ce qui rend vos attaques plus puissantes et plus rapides. Cependant, cela rend moindre votre capacité défensive.
buff-mysterious = Effet mystérieux
# Lifesteal
buff-title-lifesteal = Voleur de vie
# Sunderer
buff-title-defiance = Bravoure
# Agility
buff-title-agility = Agilité
# Agility
buff-desc-agility = Vos mouvements sont plus rapides, mais vous infligez moins de dégâts, et prenez plus de dégâts.
# Agility
buff-stat-agility =
    Augmente votre vitesse de déplacement
    de { $strength }%.
    Mais réduit vos dégâts de 100%,
    et augmente les dégâts reçus de 100%.
# Heatstroke
buff-desc-heatstroke = Vous avez été exposé à la chaleur et vous avez pris un coup de chaud. Votre récupération d'énergie et vitesse de mouvement sont réduits. Détendez vous.
# Flame
buff-title-flame = Enflammé
# Heatstroke
buff-title-heatstroke = Coup de chaleur
